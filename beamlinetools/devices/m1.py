from ophyd import PVPositioner, EpicsSignal, EpicsSignalRO, Device
from ophyd import FormattedComponent as FCpt
from ophyd import Component as Cpt


class M1Axis(PVPositioner):

    setpoint = FCpt(EpicsSignal,    '{self.prefix}{self._ch_name}',kind='normal')
    readback = FCpt(EpicsSignalRO,  '{self.prefix}{self._ch_name}.RBV', kind='hinted')
    done = FCpt(EpicsSignalRO,  '{self.prefix}{self._ch_name}.MOVN', kind='omitted')
    done_value = 0
    stop_signal = FCpt(EpicsSignal,  '{self.prefix}{self._ch_name}.STOP', kind='omitted')



    # running_signal = FCpt(EpicsSignalRO,  '{self.prefix}Run{self._ch_name}',kind='omitted')
    def __init__(self, prefix, ch_name=None, **kwargs):
        self._ch_name = ch_name
        super().__init__(prefix, **kwargs)
        self.readback.name = self.name

class M1(Device):

    tx = Cpt(M1Axis, '', ch_name='M6', labels={"mirrors"})
    rx = Cpt(M1Axis, '', ch_name='M7', labels={"mirrors"})
    ry = Cpt(M1Axis, '', ch_name='M8', labels={"mirrors"})
    rz = Cpt(M1Axis, '', ch_name='M9', labels={"mirrors"})
