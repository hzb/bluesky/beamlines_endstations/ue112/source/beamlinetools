import csv
import time

from ophyd import Component as Cpt
from ophyd import FormattedComponent as FCpt
from ophyd import EpicsSignal, EpicsSignalRO, Device, PVPositioner

from ophyd import (Component as Cpt)
# from beamlinetools.utils.file_helper import import_csv, update_low_limit, update_high_limit

from bessyii_devices.positioners import PVPositionerComparator

class UserPositionerAxis(PVPositionerComparator):

    setpoint     = FCpt(EpicsSignal,  '{self.prefix}UserSetPoint{self._ch_name}',   kind='normal')
    readback     = FCpt(EpicsSignalRO,'{self.prefix}UserSetPointRB{self._ch_name}', kind='hinted')
    offset       = FCpt(EpicsSignal,  '{self.prefix}Offset{self._ch_name}',         kind='normal')
    abs_readback = FCpt(EpicsSignalRO,'{self.prefix}PositionRB{self._ch_name}',     kind='normal')
    _rb_value    = FCpt(EpicsSignalRO,  'UE49fkk:{self._rb_name}{self._ch_name}', kind='omitted')
    stop_signal  = FCpt(EpicsSignal,  'UE49fkk:Stop', kind='omitted')
    
    status_pv       = FCpt(EpicsSignalRO,  'UE49fkk:Status',     kind='omitted')
    status_value = 1 # This is the value of the status when the motors are in position
    
    busy_ioc     = FCpt(EpicsSignal,  '{self.prefix}ActualBusy{self._ch_name}',     kind='omitted')

    # limits_file_path = '/opt/bluesky/beamlinetools/beamlinetools/reflectometer_calibration/absolute_limits.csv'
    
    def describe(self):
        ret = super().describe()
        ret[self.readback.name]['precision'] = 4
        return ret

    # @property
    # def absolute_low_limit(self):
    #     return self._absolute_low_limit

    # @property
    # def absolute_high_limit(self):
    #     return self._absolute_high_limit
    
    # @absolute_low_limit.setter
    # def absolute_low_limit(self, value):
    #     self._absolute_low_limit = value
    #     update_low_limit(self.limits_file_path, self.name, self._absolute_low_limit)

    # @absolute_high_limit.setter
    # def absolute_high_limit(self, value):
    #     self._absolute_high_limit = value
    #     update_high_limit(self.limits_file_path, self.name, self._absolute_high_limit)

    def __init__(self, prefix, ch_name=None, rb_name=None, atol=0.01,**kwargs):
        self._ch_name = ch_name
        self._rb_name = rb_name
        super().__init__(prefix, **kwargs)
        self.readback.name = self.name
        self.atol = atol

        # df = import_csv(self.limits_file_path)
        # self.absolute_low_limit = df[self.name][0]
        # self.absolute_high_limit = df[self.name][1]


    # def stop(self, success=False):
    #     # This is to avoid tripod to go in undefined state 1216
    #     # because `move` calles `stop` at the end to ensure that 
    #     # motors are stopped. Tripod is a soft-motor, this logic
    #     # is not working
    #     if self.busy_ioc.get() != self.done_value:
    #         super().stop(success=success)
        

    def set_to_zero(self):
        """Set the user position to zero by updating the offset accordingly
        """
        self.set_current_position(0)

    def set_current_position(self, value):
        """Set the user position to a value by updating the offset accordingly

        Args:
            value (int, float): the new readback value
        """        
        if (type(value) is float) or (type(value) is int):
            new_offset = value - self.abs_readback.get()
            self.offset.put(new_offset)
            time.sleep(.5)
            print(f'Offset: {self.offset.get()}')
            print(f"{self.name} is set to: {self.readback.get()}")
        else :
            raise ValueError(f"Value type is not allowed. Valid types are float or int, you are using {type(value)}.")
    
    def move(self, position, wait=True, timeout=None, moved_cb=None):
        # check if in limits, otherwise abort
        absolute_position = position - self.offset.get()
        # if self.absolute_low_limit <= absolute_position <= self.absolute_high_limit:
        #     return super().move(position, moved_cb=moved_cb, timeout=timeout)
        # else:
        #     raise ValueError(f"Movement not allowed. Absolute position will be outside limits! Current limits: {self.absolute_low_limit}, {self.absolute_high_limit} - Selected position : {absolute_position}")
        return super().move(position, moved_cb=moved_cb, timeout=timeout)

    def done_comparator(self, readback, setpoint):
        # check if readback value is inside valid range
        has_arrived = setpoint-self.atol < readback < setpoint+self.atol
        # return false as long as we detect movement
        if not has_arrived:
            return False
        else: 
            if (self.status_pv.get() == self.status_value) and (self.busy_ioc.get() == 1): 
                self.busy_ioc.put(0)
            return True

class ExperimentalChamber(Device):
    
    x = Cpt(UserPositionerAxis, '', ch_name='dX', rb_name="", atol=0.005, labels={"expchamber"})
    y = Cpt(UserPositionerAxis, '', ch_name='dY', rb_name="", atol=0.005, labels={"expchamber"})
    z = Cpt(UserPositionerAxis, '', ch_name='dZ', rb_name="", atol=0.005, labels={"expchamber"})
    r = Cpt(UserPositionerAxis, '', ch_name='rotSample', atol=0.005, labels={"expchamber"})
    read_attrs       = ['x.readback', 'y.readback', 'z.readback', 'r.readback']