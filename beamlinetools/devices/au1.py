from ophyd import FormattedComponent as FCpt
from ophyd import PVPositioner, EpicsSignal, EpicsSignalRO, Device
from bessyii_devices.positioners import PVPositionerComparator
from ophyd import Component as Cpt


class AxisTypeD(PVPositioner):
    setpoint = FCpt(EpicsSignal,    '{self.prefix}.VAL', kind='normal')
    readback = FCpt(EpicsSignalRO,  '{self.prefix}.RBV', kind='normal')
    done = FCpt(EpicsSignalRO,  '{self.prefix}.DMOV', kind='omitted')
    done_value = 1

    def __init__(self, prefix, **kwargs):
        super().__init__(prefix, **kwargs)
        self.readback.name = self.name

class AU1UE112(Device):
    # _default_read_attrs = ['top.readback', 'bottom.readback', 'left.readback', 'right.readback']
    top         = Cpt(AxisTypeD, 'Top')
    bottom      = Cpt(AxisTypeD, 'Bottom')
    left        = Cpt(AxisTypeD, 'Left') 
    right       = Cpt(AxisTypeD, 'Right') 