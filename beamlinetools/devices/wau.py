
from ophyd import Component as Cpt
from ophyd import EpicsSignal
from bessyii_devices.axes import AxisTypeD
from ophyd import Device

# the stop_signal works manuallz but not with ctrl+c
class AxysTypeDOptics(AxisTypeD):
    stop_signal     = Cpt(EpicsSignal,  '.STOP', kind='omitted')

    def __init__(self, prefix, **kwargs):
        super().__init__(prefix, **kwargs)
        self.readback.name = self.name

# WA1
class WAU(Device):
    #_default_read_attrs = ['top.readback', 'bottom.readback', 'left.readback', 'right.readback']
    top         = Cpt(AxysTypeDOptics, 'Top', labels={"apertures"})
    bottom      = Cpt(AxysTypeDOptics, 'Bottom', labels={"apertures"})
    left        = Cpt(AxysTypeDOptics, 'Left', labels={"apertures"}) # wall in old convention
    right       = Cpt(AxysTypeDOptics, 'Right', labels={"apertures"}) #ring in old convention



