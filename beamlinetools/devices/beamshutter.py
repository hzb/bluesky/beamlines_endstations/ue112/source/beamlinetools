from ophyd import Device, Component as Cpt
from ophyd import EpicsSignalRO


class Beamshutter(Device):
    status  = Cpt(EpicsSignalRO, 'BSR01U013L:Closed', labels={"beamshutter", "apertures"})
