devices_dict = {
    "accelerator": {
        'axes' : {
                'next_injection': ['time_next_injection', 'time_last_injection'],

        },
        'device_type' : 'motor',
        'headers' : ['next_injection', 'last_injection']
    },
    
    "valves": {
        'axes' : {
                'bs': ['status'],
                'v3': ['readback'],
                'v4': ['readback'],
                'v5': ['readback'],
                'v6': ['readback'],
                'v8': ['readback'],
                'v9': ['readback'],
                'v10': ['readback'],
                'v13': ['readback'],
        },
        'device_type' : 'motor',
        'headers' : ['readback']
    },
    "asbl": {
        'axes' : {
                'asbl.top': ['readback'],
                'asbl.bottom': ['readback'],
                'asbl.left': ['readback'],
                'asbl.right': ['readback'],
        },
        'device_type' : 'motor',
        'headers' : ['readback']
    },
    "m1": {
        'axes' : {
                'm1.tx': ['readback'],
                'm1.rx': ['readback'],
                'm1.ry': ['readback'],
                'm1.rz': ['readback'],
        },
        'device_type' : 'motor',
        'headers' : ['readback']
    },
    "pgm": {
        'axes' : {
                'pgm.en': ['readback'],
                'pgm.cff': [''],
                'pgm.grating': [''],
                'pgm.beta': ['readback'],
                'pgm.theta': ['readback'],
                'pgm.alpha': ['readback'],
                'pgm.slit': ['readback'],
        },
        'device_type' : 'motor',
        'headers' : ['readback']
    },
    "keithleys" : {
        'axes' :{
                'kth1': ['readback', 'nplc', 'int_time'],
                'kth2': ['readback', 'nplc', 'int_time'],
                'kth3': ['readback', 'nplc', 'int_time'],
        },
        'device_type' : 'detector',
        'headers' : ['readback', 'nplc', 'integration_time']
    },
    "expchamber" : {
        'axes' :{
                'expchamber.x': ['readback', 'abs_readback', 'offset'],
                'expchamber.y': ['readback', 'abs_readback', 'offset'],
                'expchamber.z': ['readback', 'abs_readback', 'offset'],
                'expchamber.r': ['readback', 'abs_readback', 'offset'],
        },
        'device_type' : 'motor',
        'headers' : ['readback', 'abs_readback', 'offset']
    },

}
